package edu.stts;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.cloud.firestore.SetOptions;

/**
 * Servlet implementation class EditProfile
 */
@WebServlet("editprofile")
public class EditProfile extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditProfile() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		

		  PrintWriter out = response.getWriter();
		  HttpSession session = request.getSession();
		  String email_login =(String) session.getAttribute("login_email");

		  out.print(email_login);
		  response.setContentType("application/json");
		  response.setCharacterEncoding("UTF-8");
		  
		  
		  FirestoreReference model_User = new FirestoreReference();
		  HashMap<String, Object> userHashMap = new HashMap<String, Object>();
		  	userHashMap.put("firstname", request.getParameter("firstname"));
	        userHashMap.put("lastname", request.getParameter("lastname"));
	        userHashMap.put("username", request.getParameter("username"));
	        userHashMap.put("desc", request.getParameter("desc"));
	        model_User.getDb().collection("users").document(email_login).set( userHashMap ,SetOptions.merge());
	        
	        session.setAttribute("login_username",request.getParameter("username"));
	        response.sendRedirect("index.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

