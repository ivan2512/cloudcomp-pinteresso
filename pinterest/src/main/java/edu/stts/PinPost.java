package edu.stts;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.ProcessBuilder.Redirect;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.cloud.firestore.SetOptions;

/**
 * Servlet implementation class EditProfile
 */
@WebServlet("pin")
public class PinPost extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PinPost() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		 PrintWriter out = response.getWriter();
		 HttpSession session = request.getSession();
		 String email_login =(String) session.getAttribute("login_email");
		 //out.print(email_login);
		 String urlString = request.getParameter("url");
		 //out.print(urlString);
		 FirestoreReference model_pin = new FirestoreReference();
		 Map<String, Object> pin = new HashMap<>();
		 pin.put("by", email_login);
		 pin.put("url", urlString);
		 model_pin.getDb().collection("pin").document().set(pin);
		 response.sendRedirect("index.jsp");
	}

}

