package edu.stts;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.FieldValue;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.storage.Acl;
import com.google.cloud.storage.Acl.Role;
import com.google.cloud.storage.Acl.User;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;

/**
 * Servlet implementation class ServletPost
 */
@WebServlet("user/newPost")
@MultipartConfig
public class ServletPost extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public ServletPost() {
        super();
    }
       
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String bucketName = "cloudcompfirestore";
		FirestoreReference reference = new FirestoreReference();
		Firestore db = reference.getDb();
		HttpSession sess = request.getSession();
		String email = sess.getAttribute("login_email").toString();
    final Part filePart = request.getPart("image");
    final String fileName = getFileName(filePart);
		
		DocumentReference baru = db.collection("posts").document();
		String idBaru = baru.getId();
    Post newPost = new Post();
    
    String nama = idBaru+"."+FilenameUtils.getExtension(fileName);
    
    newPost.setBy(email);
    newPost.setUrl("https://storage.googleapis.com/cloudcompfirestore/"+nama);
    
    List<String> tags = new ArrayList<String>();
    
    
    
    for (String string : request.getParameter("tags").toString().split("-")) {
      if (!tags.contains(string)) {
        tags.add(string);
      }
    }
    newPost.setTitle(request.getParameter("title").toString());
    newPost.setTags(tags);
    
    baru.set(newPost);
    
		
    BlobId blobId = BlobId.of(bucketName, nama);
    
    BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType(filePart.getContentType()).build();
    
    InputStream filecontent = filePart.getInputStream();
    byte[] fileAsByteArray = IOUtils.toByteArray(filecontent);
    
    Blob blob = new StorageReference().getStorage().create(blobInfo, fileAsByteArray, Storage.BlobTargetOption.doesNotExist());
    Acl acl = new StorageReference().getStorage().createAcl(blobId, Acl.of(User.ofAllUsers(), Role.READER));
    
    DocumentReference myReference = db.collection("users").document(email);
    
    try {
      ArrayList<String> followers = (ArrayList<String>) myReference.get().get().get("follower");
      if (followers != null) {
        for (String follower: followers) {
          DocumentReference them = db.collection("users").document(follower);
          them.update("new", FieldValue.arrayUnion(idBaru));
        }
      }
    } catch (InterruptedException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (ExecutionException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    //DEBUGGING ONLY
//    PrintWriter printer = response.getWriter();
//    response.setContentType("application/json");
    
//    JSONObject object = new JSONObject();

//    object.put("response", newPost);
//    object.put("obj", baru);
//    object.put("tags", request.getParameter("tags"));
//    response.getWriter().write(object.toString());
    response.sendRedirect("../index.jsp");
	}
	
	private String getFileName(final Part part) {
    final String partHeader = part.getHeader("content-disposition");
    for (String content : part.getHeader("content-disposition").split(";")) {
        if (content.trim().startsWith("filename")) {
            return content.substring(
                    content.indexOf('=') + 1).trim().replace("\"", "");
        }
    }
    return null;
}

}
