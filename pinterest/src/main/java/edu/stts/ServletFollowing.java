package edu.stts;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.FieldValue;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.SetOptions;
import com.google.gson.Gson;

/**
 * Servlet implementation class EditProfile
 */
@WebServlet("follow")
public class ServletFollowing extends HttpServlet {
	private static final long serialVersionUID = 1234L;
       
  public ServletFollowing() {
      super();
  }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		  
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	  
	  PrintWriter out = response.getWriter();
	  response.setContentType("text");
	  
	  String method = request.getParameter("method");
	  String target = request.getParameter("target");
	  String me = request.getSession().getAttribute("login_email").toString();
	  Firestore db = new FirestoreReference().getDb();
	    DocumentReference targetReference = db.collection("users").document(target);
	    DocumentReference myReference = db.collection("users").document(me);
      if (method.equalsIgnoreCase("follow")) {
       myReference.update("following", FieldValue.arrayUnion(target));
       targetReference.update("follower", FieldValue.arrayUnion(me));
       out.write("<button class=\"btn btn-default mb-2\" onclick=\"unfollow('"+target+"')\">Unfollow</button>");

      } else if (method.equalsIgnoreCase("unfollow")) {
        myReference.update("following", FieldValue.arrayRemove(target)); 
        targetReference.update("follower", FieldValue.arrayRemove(me));
        out.write("<button class=\"btn btn-danger mb-2\" onclick=\"follow('"+target+"')\">Follow</button>");
      }	  
	  out.flush();
	}

}

