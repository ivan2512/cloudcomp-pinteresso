package edu.stts;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.cloud.firestore.Firestore;

/**
 * Servlet implementation class ServletViewPost
 */
@WebServlet("/post")
public class ServletViewPost extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletViewPost() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	  
		response.setContentType("text/html");
		getServletContext().getRequestDispatcher("/navigation.jsp").include(request, response);
    getServletContext().getRequestDispatcher("/post.jsp").include(request, response);
    
	}

}
