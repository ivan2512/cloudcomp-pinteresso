package edu.stts;

import java.io.FileInputStream;

import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.FirestoreOptions;

public class FirestoreReference {
  private Firestore db;
  private static final String projectid = "cloudcomp-firestore";
  private static final String namajson = "CloudComp Firestore-7e1411d35ac7.json";
  
  public FirestoreReference() {
    try {
      db = FirestoreOptions.newBuilder()
          .setProjectId(projectid)
          .setCredentials(
              ServiceAccountCredentials
              .fromStream(new FileInputStream(namajson))
              )
          .build()
          .getService();
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }

  public Firestore getDb() {
    return db;
  }
  
}
