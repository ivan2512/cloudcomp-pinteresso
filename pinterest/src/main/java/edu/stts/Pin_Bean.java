package edu.stts;

import java.util.List;
import java.util.concurrent.ExecutionException;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;

public class Pin_Bean {
  FirestoreReference ref = new FirestoreReference();
  StorageReference sRef = new StorageReference();
  List<QueryDocumentSnapshot> documents;
  
  public Pin_Bean() throws InterruptedException, ExecutionException {
    //asynchronously retrieve all documents
    ApiFuture<QuerySnapshot> future = ref.getDb().collection("pin").get();
    // future.get() blocks on response
    documents = future.get().getDocuments();
  }

  public List<QueryDocumentSnapshot> getDocuments() {
    return documents;
  }
}
