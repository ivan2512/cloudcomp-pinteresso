package edu.stts;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.gson.Gson;

/**
 * Servlet implementation class Login
 */
@WebServlet("user/login")
public class ServletLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletLogin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		PrintWriter out = response.getWriter();

		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		Map<String, String[]> map = request.getParameterMap();
		Set<Entry<String, String[]>> set = map.entrySet();
		
			  
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		//out.print("password" + password);
		
		FirestoreReference model_User = new FirestoreReference();
		
		DocumentReference docRef = model_User.getDb().collection("users").document(email);
		// asynchronously retrieve the document
		ApiFuture<DocumentSnapshot> future = docRef.get();
		// block on response
		DocumentSnapshot document;
		try {
			document =future.get();
			if (document.exists()) {
				  // convert document to POJO
				  String passdb = document.getString("password");
				  //out.print("passdb" + passdb);
				  if(passdb.equalsIgnoreCase(password)) {
					HttpSession session= request.getSession();
					
					//dapatkan username nya dulu
					String username = document.getString("username");
					
					session.setAttribute("login_username", username);
					session.setAttribute("login_email", email);
					Gson gson = new Gson();
			        HashMap<String, Object> kembalian = new HashMap<String, Object>();
			        kembalian.put("success", true);
			        kembalian.put("message", "Login Succesfully");
			        response.getWriter().write(gson.toJson(kembalian));
			        response.getWriter().flush();
				  }else {
					  Gson gson = new Gson();
				        HashMap<String, Object> kembalian = new HashMap<String, Object>();
				        kembalian.put("success", false);
				        kembalian.put("message", "Login Failed");
				        response.getWriter().write(gson.toJson(kembalian));
				        response.getWriter().flush();
				  }
				  
				} else {
				  System.out.println("Login Failed!");
				}
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
