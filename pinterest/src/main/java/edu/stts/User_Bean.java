package edu.stts;
import java.util.concurrent.ExecutionException;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;

public class User_Bean {
	FirestoreReference model_User= new FirestoreReference();
	
	public String getFirstName(String email) throws InterruptedException, ExecutionException {
		DocumentReference docRef = model_User.getDb().collection("users").document(email);
	  	ApiFuture<DocumentSnapshot> future = docRef.get();
		DocumentSnapshot document;
		document= future.get();
		return document.getString("firstname");
	}
	
	public String getLastName(String email) throws InterruptedException, ExecutionException {
		DocumentReference docRef = model_User.getDb().collection("users").document(email);
	  	ApiFuture<DocumentSnapshot> future = docRef.get();
		DocumentSnapshot document;
		document= future.get();
		return document.getString("lastname");
	}
	
	public String getDesc(String email) throws InterruptedException, ExecutionException {
		DocumentReference docRef = model_User.getDb().collection("users").document(email);
	  	ApiFuture<DocumentSnapshot> future = docRef.get();
		DocumentSnapshot document;
		document= future.get();
		return document.getString("desc");
	}
	
	public String getUsername(String email) throws InterruptedException, ExecutionException {
	    DocumentReference docRef = model_User.getDb().collection("users").document(email);
	    ApiFuture<DocumentSnapshot> future = docRef.get();
	    DocumentSnapshot document;
	    document= future.get();
	    return document.getString("username");
	}
}
