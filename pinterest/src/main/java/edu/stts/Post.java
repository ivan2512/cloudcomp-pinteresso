package edu.stts;

import java.time.LocalDate;
import java.util.List;

import com.google.cloud.firestore.FieldValue;

public class Post {
  private String title;
  private String by;
  private Object timestamp;
  private List<String> tags;
  private String url;
  
  public Post() {
    timestamp = FieldValue.serverTimestamp();
  }

  public Post(String by, LocalDate postedAt, List<String> tags, String url, String title) {
    super();
    this.by = by;
    this.timestamp = postedAt;
    this.tags = tags;
    this.url = url;
    this.title = title;
  }

  public String getBy() {
    return by;
  }

  public void setBy(String by) {
    this.by = by;
  }

  public Object getPostedAt() {
    return timestamp;
  }

  public void setPostedAt(Object postedAt) {
    this.timestamp = postedAt;
  }

  public List<String> getTags() {
    return tags;
  }

  public void setTags(List<String> tags) {
    this.tags = tags;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }
  
}
