package edu.stts;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.cloud.firestore.SetOptions;

/**
 * Servlet implementation class EditProfile
 */
@WebServlet("profile")
public class Profile extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Profile() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		  PrintWriter out = response.getWriter();
		  HttpSession session = request.getSession();
		  String email = (String) request.getParameter("email"); 
		  String email_login = (String) session.getAttribute("login_email");
		  //out.println("Email Login : " + email_login);
		  if(email.equals(email_login))
		  {
			  response.sendRedirect("author.jsp");
			  //out.println("sama");
		  }
		  else
		  {
			  response.sendRedirect("viewprofile.jsp?email="+email+"");
			  //out.println("Email : " + email);
		  }
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

