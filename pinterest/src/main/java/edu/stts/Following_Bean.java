package edu.stts;

import java.util.List;
import java.util.concurrent.ExecutionException;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;

public class Following_Bean {
  Firestore ref;
  
  public Following_Bean() {
    ref = new FirestoreReference().getDb();
  }
  
  public Object getUserFollowing(String email) throws InterruptedException, ExecutionException {
		DocumentReference docRef = ref.collection("users").document(email);
		ApiFuture<DocumentSnapshot> future = docRef.get();
		return future.get().get("following");		
	}
  
  public Object getUserFollower(String email) throws InterruptedException, ExecutionException {
    DocumentReference docRef = ref.collection("users").document(email);
    ApiFuture<DocumentSnapshot> future = docRef.get();
    return future.get().get("follower");   
  }
}
