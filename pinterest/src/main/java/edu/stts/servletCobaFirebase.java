package edu.stts;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.api.gax.paging.Page;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;

@WebServlet(
    name = "HelloAppEngine",
    urlPatterns = {"/cobafirebase"}
)
public class servletCobaFirebase extends HttpServlet {
  private static final long serialVersionUID = 1412L;
  private Firestore db;
  private static final String projectid = "cloudcomp-firestore";
  private static final String namajson = "CloudComp Firestore-7e1411d35ac7.json";
  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response) 
      throws IOException, ServletException {
    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    
    Storage storage = StorageOptions.newBuilder()
        .setProjectId(projectid)
        .setCredentials(ServiceAccountCredentials.fromStream(new FileInputStream(namajson)))
        .build()
        .getService();

    out.println("Bucket list:<br/>");
    Page<Bucket> buckets = storage.list();
    Iterable<Bucket> bucketIterator = buckets.iterateAll();
    for (Bucket bucket : bucketIterator) {
      out.println(bucket.getName() + "<br/>");    
    }
    
    String bucketName = "cloudcompfirestore";
    
    out.println("Blob list:<br/>");
    Page<Blob> blobs = storage.list(bucketName);
    Iterable<Blob> blobIterator = blobs.iterateAll();
    for (Blob b : blobIterator) {
      out.println(b.getName() + "<br/>");   
    }
    
    //$this -> load-> view("index.jsp")
    //request.getRequestDispatcher("/index.jsp").include(request, response);
// FIREBASE
//    try {
//      out.print("TRY<br>");
//      db = FirestoreOptions.newBuilder()
//          .setProjectId(projectid)
//          .setCredentials(
//              ServiceAccountCredentials
//              .fromStream(new FileInputStream(namajson))
//              )
//          .build()
//          .getService();
//    } catch (Exception e) {
//      out.print("CATCH <br>");
//      out.print(e.getMessage());
//    }
    
//    response.setContentType("text/plain");
//    response.setCharacterEncoding("UTF-8");
//
//    response.getWriter().print("Hello App Engine!\r\n");
    
  }
}
