package edu.stts;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Query;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.cloud.firestore.CollectionReference;


public class Post_Bean {
  FirestoreReference ref = new FirestoreReference();
  StorageReference sRef = new StorageReference();
  
  
  public Post_Bean()  {
    
  }

  public List<QueryDocumentSnapshot> getDocuments() throws InterruptedException, ExecutionException{
    List<QueryDocumentSnapshot> documents;
    ApiFuture<QuerySnapshot> future = ref.getDb().collection("posts").get();
    documents = future.get().getDocuments();
    return documents;
  } 
  
  public DocumentSnapshot getSingleDoc(String uid) throws InterruptedException, ExecutionException {
    return ref.getDb().collection("posts").document(uid).get().get();
  }
  
  public List<QueryDocumentSnapshot> getDocumentsByTags(String param) throws InterruptedException, ExecutionException {
    List<QueryDocumentSnapshot> documents;
    CollectionReference postRef =  ref.getDb().collection("posts");
    
    
    documents = postRef.whereArrayContainsAny("tags", Arrays.asList(param.split(" "))).get().get().getDocuments();
    return documents;
  }
}
