package edu.stts;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.gson.Gson;



/**
 * Servlet implementation class ControllerUser
 */
@WebServlet("/user/register")
public class ServletRegister extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public ServletRegister() {
      super();
  }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	  response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");
	  Map<String, String[]> map = request.getParameterMap();
	  Set<Entry<String, String[]>> set = map.entrySet();
	  
	  String emailBaru = request.getParameter("email");
	  
	  FirestoreReference model_User = new FirestoreReference();
	  
	  DocumentReference docRef = model_User.getDb().collection("users").document(emailBaru);
  	// asynchronously retrieve the document
  	ApiFuture<DocumentSnapshot> future = docRef.get();
  	// ...
  	// future.get() blocks on response
  	
  	DocumentSnapshot document;
    try {
      document = future.get();
      if (document.exists()) {
        Gson gson = new Gson();
        HashMap<String, Object> kembalian = new HashMap<String, Object>();
        kembalian.put("success", false);
        kembalian.put("message", "Email is already used");
        response.getWriter().write(gson.toJson(kembalian));
        response.getWriter().flush();
      } else {
        HashMap<String, Object> userHashMap = new HashMap<String, Object>();
        String username, email;
        email = emailBaru;
        username = emailBaru.substring(0, emailBaru.indexOf("@"));
        userHashMap.put("email", email);
        userHashMap.put("username", username);
        userHashMap.put("password", request.getParameter("password"));
        userHashMap.put("age", request.getParameter("age"));
        model_User.getDb().collection("users").document(emailBaru).set( userHashMap );
        
        Gson gson = new Gson();
        HashMap<String, Object> kembalian = new HashMap<String, Object>();
        kembalian.put("success", true);
        kembalian.put("message", "Registered succesfully");
        response.getWriter().write(gson.toJson(kembalian));
        response.getWriter().flush();
        
        HttpSession session= request.getSession();
        session.setAttribute("login_username", username);
        session.setAttribute("login_email", email);
      }
    } catch (InterruptedException e) {
      e.printStackTrace();
    } catch (ExecutionException e) {
      e.printStackTrace();
    }
  	
	 
	}

}
