package edu.stts;

import java.io.FileInputStream;

import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;

public class StorageReference {
  private Storage db;
  private static final String projectid = "cloudcomp-firestore";
  private static final String namajson = "CloudComp Firestore-7e1411d35ac7.json";

  public StorageReference() {
    try {
      db = StorageOptions.newBuilder()
          .setProjectId(projectid)
          .setCredentials(ServiceAccountCredentials.fromStream(new FileInputStream(namajson)))
          .build()
          .getService();
    } catch (Exception e) {
    }
  }
  
  public Storage getStorage() {
    return db;
  }
}
