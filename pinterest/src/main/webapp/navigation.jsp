<%@page import="java.awt.Window"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
	<style>
		.modal-backdrop{
			display: none; 
		}
		.modal{
			text-align: center;
		}
	</style>
    <title>CC2019 - Pinterest</title>
    <script type="text/javascript"> (function() { var css = document.createElement('link'); css.href = 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'; css.rel = 'stylesheet'; css.type = 'text/css'; document.getElementsByTagName('head')[0].appendChild(css); })(); </script>
    <link rel="stylesheet" href="assets/css/app.css">
    <link rel="stylesheet" href="assets/css/theme.css">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
<meta charset="ISO-8859-1">
<script src="assets/js/app.js"></script>
</head>
<body>
<jsp:useBean id="userbean" class="edu.stts.User_Bean"></jsp:useBean>
    <nav class="navbar navbar-expand-lg navbar-light bg-white fixed-top">
    <a class="navbar-brand font-weight-bolder mr-3" href="index.jsp"><img src="assets/img/logo.png"></a>
    <button class="navbar-light navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsDefault" aria-controls="navbarsDefault" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarsDefault">
    	<ul class="navbar-nav mr-auto align-items-center">
    		<li>
    		<form class="bd-search hidden-sm-down" method="GET" action="/search">
                <input type="text" name="query" class="form-control bg-graylight border-0 font-weight-bold" id="search-input" placeholder="Search..." autocomplete="off">
            </form>
            </li>
    	</ul>
    	<ul class="navbar-nav ml-auto align-items-center">
    		<li class="nav-item">
    		<a class="nav-link" href="index.jsp">Home</a>
    		</li>
    		<li class="nav-item">
    		<a class="nav-link" onclick="ceklogin()" id="namalogin"><img class="rounded-circle mr-2" src="assets/img/av.png" width="30"><span class="align-middle">
    		<%if(session.getAttribute("login_username")!=null){
        		out.print(session.getAttribute("login_username")); 
    		}%></span>
    		</a>
    		</li>
    		<%if (session.getAttribute("login_username") != null){ %>
    		  <li class="nav-item">
    		      <a href="following.jsp" class="nav-link">Notifications</a>
    		  </li>
    		<% } %>
    		<li class="nav-item dropdown">
    		<a class="nav-link" href="#" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">
    		<svg style="margin-top:10px;" class="_3DJPT" version="1.1" viewbox="0 0 32 32" width="21" height="21" aria-hidden="false" data-reactid="71"><path d="M7 15.5c0 1.9-1.6 3.5-3.5 3.5s-3.5-1.6-3.5-3.5 1.6-3.5 3.5-3.5 3.5 1.6 3.5 3.5zm21.5-3.5c-1.9 0-3.5 1.6-3.5 3.5s1.6 3.5 3.5 3.5 3.5-1.6 3.5-3.5-1.6-3.5-3.5-3.5zm-12.5 0c-1.9 0-3.5 1.6-3.5 3.5s1.6 3.5 3.5 3.5 3.5-1.6 3.5-3.5-1.6-3.5-3.5-3.5z" data-reactid="22"></path></svg>
    		</a>
			
			<%if(session.getAttribute("login_username")!=null){%>
				<div class="dropdown-menu dropdown-menu-right shadow aria-labelledby="dropdown02" id="exampleModal">
	    			
	    			<span class="nav-item dropdown">
	    			<a class="nav-link" href="profile.jsp" >Edit Profile</a>
	    			</span>
	    			<span class="nav-item dropdown">
	    				<a class="nav-link" href="#" onclick="dologout()" >Logout</a>
	    			</span>
				</div> 
			<%}else{ %>
				<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog " role="document" >
					<div class="modal-content">
						<div class="modal-header ">
							<h4 class="modal-title" id="exampleModalLabel">Welcome to Pinterest</h4>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<form id="formRegister">
							<div class="form-group">
								<input type="text" class="form-control" name="email" placeholder="Email">
							</div>
							<div class="form-group">
								<input  type="password" class="form-control" name="password" placeholder="Create Password">
							</div>
							<div class="form-group">
									<input  type="number" class="form-control" id="age" placeholder="Age" name="age" min="1">
								</div>
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary" onclick="lanjutRegister()">Continue</button>
						</div>
						<div class="modal-body   ">
						<h4 class="modal-title text-center">OR</h4>
							<form >
							
							<div class="form-group">
								<a class="nav-link" href="#" id="modalregister" data-toggle="modal" data-target="#LoginModal" data-whatever="@mdo">
					    			<button type="button" class="btn btn-primary " data-dismiss="modal"  >Login Now</button>
					    		</a>
							</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			
			
			<div class="modal fade" id="LoginModal" tabindex="-1" role="dialog" aria-labelledby="LoginModalLabel" aria-hidden="true">
				<div class="modal-dialog " role="document" >
					<div class="modal-content">
						<div class="modal-header ">
							<h4 class="modal-title" id="exampleModalLabel">Login</h4>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						
						<form id='formLogin'>
						<div class="modal-body">
							<div class="form-group">
								<input type="text" class="form-control" name='email' placeholder="Email">
							</div>
							
							<div class="form-group">
								<input  type="password" class="form-control" name='password' placeholder="Password">
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" id="btnClose" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary" onclick="lanjutLogin()">Login</button>
						</div>
						</form>
					</div>
				</div>
			</div>
			<% } %>
			
    		</li>
    	</ul>
    </div>
    </nav>    
<script>
  function lanjutRegister() {
	let data = $("#formRegister").serialize();
	console.log(data);
	$.ajax({
	  url:"user/register",
	  type:"POST",
	  data:data,
	  success:(result)=>{
		if (result.success){
			window.location = "profile.jsp";
		} else {
			alert("Registration failed!\nEmail address is tied to another account");
		}
	  }
	});
	//$.ajax to servlet
  }
  
  function lanjutLogin(){
	  let data = $("#formLogin").serialize();
	  console.log(data);
	  $.ajax({
		  url:"user/login",
		  type:'POST',
		  data:data,
		  success:(result)=>{
			  console.log(result);
			  if (result.success){
				  $("#btnClose").click();
				  window.location="index.jsp";  
			  } else {
				  alert("Login failed!");
			  }
		  }
	  });
  }
  
 function ceklogin(){
<%if(session.getAttribute("login_email")==null){%>
   $("#exampleModal").modal('show');
<%}else{%>
   location.href= "author.jsp";
<%}%>
 }
 
 function dologout(){
	 $.ajax({
		 url:"user/logout",
		 type:"POST",
		 data:{"logout":true},
		 success:(result)=>{
			 console.log(result);
			 if (result){
				 window.location = "index.jsp";
			 }
		 }
	 });
 }
</script>