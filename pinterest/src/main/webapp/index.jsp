<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="java.util.List"%>
<%@page import="com.google.cloud.firestore.QueryDocumentSnapshot"%>
   
<% request.getRequestDispatcher("/navigation.jsp").include(request, response);%>
    <main role="main">
        
    
    <section class="mt-4 mb-5">
    <div class="container mb-4">
        <h1 class="font-weight-bold title">Explore</h1>
    </div>
    <div class="container-fluid mb-4">
    <jsp:useBean id="posts" class="edu.stts.Post_Bean"></jsp:useBean>
        <div class="row">
            <div class="card-columns">
    <%
    List<QueryDocumentSnapshot> temps = posts.getDocuments();
    for(QueryDocumentSnapshot docs:temps){
      %>
        <div class="card card-pin">
            <img class="card-img" src="<%=docs.getString("url")%>" alt="Card image">
            <div class="overlay">
                <h2 class="card-title title"><%=docs.getString("title")%></h2>
                <div class="more">
                    <a href="post?id=<%=docs.getId()%>">
                    <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> More </a>
                </div>
            </div>
        </div>
      <%
    }
    %>
            </div>
        </div>
    </div>
    </section>
        
    </main>
    
    <footer class="footer pt-5 pb-5 text-center">
        
    <div class="container">
        
          <div class="socials-media">
    
            <ul class="list-unstyled">
              <li class="d-inline-block ml-1 mr-1"><a href="https://facebook.com" class="text-dark"><i class="fa fa-facebook"></i></a></li>
              <li class="d-inline-block ml-1 mr-1"><a href="https://twitter.com" class="text-dark"><i class="fa fa-twitter"></i></a></li>
              <li class="d-inline-block ml-1 mr-1"><a href="https://instagram.com" class="text-dark"><i class="fa fa-instagram"></i></a></li>
            </ul>
    
          </div>
        
            <!--
              All the links in the footer should remain intact.
              You may remove the links only if you donate:
              https://www.wowthemes.net/freebies-license/
            -->
          <p>�  <span class="credits font-weight-bold">        
            <a target="_blank" class="text-dark" href="https://www.wowthemes.net/pintereso-free-html-bootstrap-template/"><u>Pintereso Bootstrap HTML Template</u> by WowThemes.net.</a>
          </span>
          </p>
    
    
        </div>
        
    </footer>    
</body>
<script src="assets/js/app.js"></script>
    
</html>
