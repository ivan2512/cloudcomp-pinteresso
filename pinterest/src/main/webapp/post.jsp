<%@page import="java.io.Writer"%>
<%@page import="com.google.cloud.firestore.DocumentSnapshot"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<jsp:useBean id="posts" class="edu.stts.Post_Bean"></jsp:useBean>
<%
  String id = request.getParameter("id");
  DocumentSnapshot post = posts.getSingleDoc(id);
%>
    <main role="main">
        
    
    <section class="bg-gray200 pt-5 pb-5">
    <div class="container">
    	<div class="row justify-content-center">
    		<div class="col-md-7">
    			<article class="card">
    			<img class="card-img-top mb-2" src="<%=post.getString("url")%>" alt="Card image">
    			<div class="card-body">
    				<h1 class="card-title display-4">
    				<%=post.getString("title") %> </h1>
    				
    				<% 
    					HttpSession sessionn = request.getSession();
    				  	String email_login =(String) sessionn.getAttribute("login_email");
    				  	if (email_login != null && !email_login.equalsIgnoreCase(post.getString("by"))){%>
    				  		<form action="pin" method="post">
	        					<input type="hidden" name="url" value="<%=post.getString("url")%>">
	        					<input type="submit" value="pin post" class="btn btn-danger">
	        				</form>
	        				<%
    				  	}
    				%>
    				<jsp:useBean id="users" class="edu.stts.User_Bean"></jsp:useBean>					
    				<h5 class="card-text"> Uploaded by <a href="profile?email=<%=post.getString("by") %>">
    				<%=users.getUsername(post.getString("by")) %> </a> </h5>
    				<small class="d-block"><a class="btn btn-sm btn-gray200" href="<%=post.getString("url")%>"><i class="fa fa-external-link"></i> View Image</a></small>
    			</div>
    			</article>
    		</div>
    	</div>
    </div>
    </section>
        
    </main>

    <script src="assets/js/app.js"></script>
    <script src="assets/js/theme.js"></script>
    
    <footer class="footer pt-5 pb-5 text-center">
        
    <div class="container">
        
          <div class="socials-media">
    
            <ul class="list-unstyled">
              <li class="d-inline-block ml-1 mr-1"><a href="#" class="text-dark"><i class="fa fa-facebook"></i></a></li>
              <li class="d-inline-block ml-1 mr-1"><a href="#" class="text-dark"><i class="fa fa-twitter"></i></a></li>
              <li class="d-inline-block ml-1 mr-1"><a href="#" class="text-dark"><i class="fa fa-instagram"></i></a></li>
              <li class="d-inline-block ml-1 mr-1"><a href="#" class="text-dark"><i class="fa fa-google-plus"></i></a></li>
              <li class="d-inline-block ml-1 mr-1"><a href="#" class="text-dark"><i class="fa fa-behance"></i></a></li>
              <li class="d-inline-block ml-1 mr-1"><a href="#" class="text-dark"><i class="fa fa-dribbble"></i></a></li>
            </ul>
    
          </div>
        
            <!--
              All the links in the footer should remain intact.
              You may remove the links only if you donate:
              https://www.wowthemes.net/freebies-license/
            -->
          <p>�  <span class="credits font-weight-bold">        
            <a target="_blank" class="text-dark" href="https://www.wowthemes.net/pintereso-free-html-bootstrap-template/"><u>Pintereso Bootstrap HTML Template</u> by WowThemes.net.</a>
          </span>
          </p>
    
    
        </div>
        
    </footer>    
</body>
    
</html>
