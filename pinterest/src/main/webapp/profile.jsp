<%@page import="edu.stts.User_Bean"%>
<%@page import="edu.stts.FirestoreReference"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<jsp:useBean id="userbean" class="edu.stts.User_Bean"></jsp:useBean>
<% request.getRequestDispatcher("/navigation.jsp").include(request, response);
	
%>
    <section class="mt-4 mb-5">
	    <div class="container mb-4">
	        <h1 class="font-weight-bold title">Profile</h1>
	        <% if( userbean.getFirstName(session.getAttribute("login_email").toString()) == null ){ %>
	          <p class="text-danger">
	           Please fill out this form to complete your registration.
	          </p>
	        <% } %>
	        <hr>
	        <form action="editprofile" method="post" >
	          
	          <div class="form-group" >
			    <label for="firstname">First Name </label>
			    <input type="text" class="form-control" name="firstname" id="firstname" placeholder="Anthony" 
			    		value="<%=userbean.getFirstName( session.getAttribute("login_email").toString() )%>">
			    <label for="lastname">Last Name </label>
			    <input type="text" class="form-control" name="lastname"  id="lastname" placeholder="Leonard" 
			    		value="<%=userbean.getLastName( session.getAttribute("login_email").toString() )%>">
			  </div>
			  <div class="form-group">
			    <label for="username">Username </label>
			    <input type="text" class="form-control" name="username" id="username" placeholder="anthony21" 
			    		value="<%out.print(session.getAttribute("login_username"));%>" >
			  </div>
			   <div class="form-group">
			    <label for="desc">About your Profile </label>
			    <textarea class="form-control" name="desc" id="desc"><%=userbean.getDesc(session.getAttribute("login_email").toString())%></textarea>
			  </div>
			  <input type="submit" class="btn btn-primary" name="done" value="Done">
			</form>
	    </div>
	    
	    
	</section>

    
   
</body>
<script src="assets/js/app.js"></script>
<script src="assets/js/theme.js"></script>
       
</html>