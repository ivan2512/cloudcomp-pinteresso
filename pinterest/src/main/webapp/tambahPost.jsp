<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<% request.getRequestDispatcher("/navigation.jsp").include(request, response);%>
<div class="container-fluid">
	<div class="row">
		<div class="col-8 offset-2 mt-3">
			<div class="card" style="border-radius: 30px;">
				<div class="card-header">
					<h4 class="card-title">New Post</h4>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-6">
							<img alt="" style="width: 100%; height: 500px" id="previewImg">
						</div>
						<div class="col-6">
							<form action="/user/newPost" enctype="multipart/form-data" id="formPost" method="POST">
							    <div class="input-group">
								  <div class="custom-file">
								    <input type="file" class="custom-file-input" id="inpImage" name="image" onchange="onImageChange(this)">
								    <label class="custom-file-label" for="inpImage" id="labelImage">Choose file</label>
								  </div>
								</div>
								<div class="form-group">
								<label for="inptitle">Title</label>
								<input type="text" class="form-control" id="inptitle" name="title" placeholder="Add your title here">
								</div>
								<div class="form-group">
								    <label for="tags">Tags</label>
								    <input type="text" class="form-control" readonly id="hiddentags" name="tags" /> <br />
								    <input type="text" class="form-control" id="tags" onkeyup="ganti(event)" />
								</div>
								<input type="submit" value="Submit" class="btn btn-danger"/>
							</form>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>
</body>
<script src="assets/js/app.js"></script>
<script>
  function onImageChange(param){
	  var img = document.querySelector("#previewImg");
      if (param.files || param.files[0]) {
          var reader = new FileReader();
          reader.readAsDataURL(param.files[0]);
          reader.onload = (e) => {
              img.setAttribute('src', e.target.result);
              $("#labelImage").html(param.files[0].name);
          }
      } else {
          img.removeAttribute('src');
          $("#labelImage").html("Choose Image");
      }
  }
  
  function newPost(){
	  var data = $("#formPost").serialize();
	  $.ajax({
		  type:"POST",
		  url:"user/newpost",
		  data:data,
		  success:(res)=>{
			  console.log(res);
		  }
	  });
  }
  
  function ganti(evt){
	  var output = $("#hiddentags");
	  var input = $("#tags");
	  if (evt.code == "Space"){
		  let tag = input.val().trim();
		  output.val(output.val() + tag + "-");
		  input.val("");
	  }
  }
</script>
</html>