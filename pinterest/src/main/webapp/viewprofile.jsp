<%@page import="com.google.cloud.firestore.QueryDocumentSnapshot"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<head>
	<style>
	.modal-backdrop{
		display: none; 
	}
	.modal{
		text-align: center;
	}
	</style>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CC2019 - Pinterest</title>
    <script type="text/javascript"> (function() { var css = document.createElement('link'); css.href = 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'; css.rel = 'stylesheet'; css.type = 'text/css'; document.getElementsByTagName('head')[0].appendChild(css); })(); </script>
    <link rel="stylesheet" href="assets/css/app.css">
    <link rel="stylesheet" href="assets/css/theme.css">

	
    <script src="assets/js/app.js"></script>
    
</head>


<body>
<jsp:useBean id="userbean" class="edu.stts.User_Bean"></jsp:useBean>
<jsp:useBean id="followbean" class="edu.stts.Following_Bean"></jsp:useBean>

<% request.getRequestDispatcher("/navigation.jsp").include(request, response); %>
   <main role="main">
    
    <div class="jumbotron border-round-0 min-50vh" 
    style="background-image:url('https://storage.googleapis.com/cloudcompfirestore/background.jpg')">
    </div>
    <div class="container mb-4">
    	<img src="assets/img/av.png" class="mt-neg100 mb-4 rounded-circle" width="128">
    	<%if(request.getParameter("email")!=null){ %>
    		<h1 class="font-weight-bold title">
    		<%=userbean.getUsername(request.getParameter("email").toString()) %>
    		</h1>
    		<p>
	    	</p>
	    	<div id="containerButton">
	    <% if (session.getAttribute("login_email") != null){
		    	  ArrayList<String> myFollowing = (ArrayList<String>)followbean.getUserFollowing(session.getAttribute("login_email").toString());
	          if (myFollowing.contains(request.getParameter("email"))){ %>
	           <button class="btn btn-default mb-2" onclick="unfollow('<%=request.getParameter("email")%>')">Unfollow</button>   
	        <% } else {%>
	            <button class="btn btn-danger mb-2" onclick="follow('<%=request.getParameter("email")%>')">Follow</button>
	        <% }
	    	}
	    	%>
	         
    		</div>
    	<%}%>
    	
    	
    </div>
    <div class="container-fluid mb-5">
		<h2 class="font-weight-bold title"> <%=userbean.getUsername(request.getParameter("email").toString())%>'s Posts </h2>
		<div class="row">
		    <div class="card-columns">
		        <jsp:useBean id="myPost" class="edu.stts.Post_Bean"></jsp:useBean>
		<%  
		List<QueryDocumentSnapshot> tPosts = myPost.getDocuments();
		for(QueryDocumentSnapshot docs:tPosts){
		    String email_user =(String) request.getParameter("email");
		    if (docs.getString("by").equals(email_user)){
		        %>
		<div class="card card-pin">
		    <img class="card-img" src="<%=docs.getString("url")%>" alt="Card image">
		    <div class="overlay">
		      <div class="more">
		        <small class="d-block"><a class="btn btn-sm" href="<%=docs.getString("url")%>"><i class="fa fa-external-link"></i> View Image</a></small>
		      </div>
		    </div>
		</div>
		<%
		    }
		}
		%>
		    </div>
		</div>
    </div>
        
   </main>

    
    <footer class="footer pt-5 pb-5 text-center">
    <div class="container">
            <!--
              All the links in the footer should remain intact.
              You may remove the links only if you donate:
              https://www.wowthemes.net/freebies-license/
            -->
          <p><span class="credits font-weight-bold">        
            <a target="_blank" class="text-dark" href="https://www.wowthemes.net/pintereso-free-html-bootstrap-template/"><u>Pintereso Bootstrap HTML Template</u> by WowThemes.net.</a>
          </span>
          </p>
        </div>
    </footer>
<script>
  $(document).ready((param)=>{
    //alert();
  });
  function unfollow(useremail){
      $.ajax({type:"post",
          data:{"target":useremail, "method":"unfollow"},
          url:"follow",
          success:(result)=>{
              $("#containerButton").html(result);
          }
      });
  }
  
  function follow(useremail){
	  $.ajax({type:"post",
          data:{"target":useremail, "method":"follow"},
          url:"follow",
          success:(result)=>{
              $("#containerButton").html(result);
          }
      });
  }
  
</script>    
</body>
    
</html>