<%@page import="com.google.cloud.firestore.DocumentSnapshot"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.google.cloud.firestore.FieldValue"%>

<% request.getRequestDispatcher("/navigation.jsp").include(request, response); %>
<jsp:useBean id="firestore" class="edu.stts.FirestoreReference"></jsp:useBean>
<jsp:useBean id="posts" class="edu.stts.Post_Bean"></jsp:useBean>
<div class="container">
<%
  String email = session.getAttribute("login_email").toString();
  Object obj = firestore.getDb().collection("users").document(email).get().get().get("new");
  if (obj instanceof ArrayList<?>){
    ArrayList<String> baru =  (ArrayList<String>) obj;
    for (String elemen : baru){
      DocumentSnapshot post = posts.getSingleDoc(elemen);
      %> <h3>New Post from <%=post.get("by")  %> </h3>
      <div class="row">
        <div class="col"><div class="card card-pin">
          <img class="card-img" src="<%=post.get("url").toString()%>" alt="Card image">
          <div class="overlay">
              <h2 class="card-title title"><%=post.get("title").toString()%></h2>
              <div class="more">
                  <a href="post?id=<%=post.getId()%>">
                  <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> Open </a>
              </div>
          </div>
      </div></div>
      </div>
      <%
      firestore.getDb().collection("users").document(email).update("new", FieldValue.arrayRemove(post.getId()));
    }
  }
%>
</div>
</body>
</html>