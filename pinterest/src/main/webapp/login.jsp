<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<% request.getRequestDispatcher("/navigation.jsp").include(request, response);%>


<div class="row">
	<div class="col-8 offset-2">
		<div class="card">
			<div class="card-header">Login</div>
			<form id='formLoginn'>
				<div class="card-body">
					<input type="text" class="form-control" name='email' placeholder="Email">
					<input  type="password" class="form-control" name='password' placeholder="Password">
				</div>
				
				<div class="card-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="lanjutLoginn()">Login</button>
			
				</div>
			</form>
		</div>
	</div>
</div>
</body>
</html>
<script>
function lanjutLoginn(){
	  let data = $("#formLoginn").serialize();
	  console.log(data);
	  $.ajax({
		  url:"user/login",
		  type:'POST',
		  data:data,
		  success:(result)=>{
			  console.log(result);
			  if (result.success){
				  window.location="post.jsp?id=<%
						
						  
						  String id = request.getParameter("id");
						  out.print(id);
						  
						  
						
				%>";
			  } else {
				  alert("Login failed!");
			  }
		  }
	  });
}

</script>