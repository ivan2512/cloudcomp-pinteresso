<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="java.util.List"%>
<%@page import="com.google.cloud.firestore.QueryDocumentSnapshot"%>

<jsp:useBean id="userbean" class="edu.stts.User_Bean"></jsp:useBean>
<% request.getRequestDispatcher("/navigation.jsp").include(request, response); %>
   <main role="main">
    
    <div class="jumbotron border-round-0 min-50vh" style="background-image:url('https://storage.googleapis.com/cloudcompfirestore/background.jpg')">
    </div>
    <div class="container mb-4">
    	<img src="assets/img/av.png" class="mt-neg100 mb-4 rounded-circle" width="128">
    	<%if(session.getAttribute("login_username")!=null){ %>
    		<h1 class="font-weight-bold title"><%out.print(session.getAttribute("login_username")); %></h1>
    		<p>
	    		<%out.print(userbean.getDesc((String)session.getAttribute("login_email")));%>
	    	</p>
	    	<a href="tambahPost.jsp"><i class="fa fa-plus" aria-hidden="true"></i></a>
    	<%}%>
    </div>
    <div class="container-fluid mb-5">
        <h2 class="font-weight-bold title"> My Pins </h2>
    	<div class="row">
            <div class="card-columns">
            	<jsp:useBean id="pin" class="edu.stts.Pin_Bean"></jsp:useBean>
		    	<%	
			    List<QueryDocumentSnapshot> temps = pin.getDocuments();
			    for(QueryDocumentSnapshot docs:temps){
			    	String email_login =(String) session.getAttribute("login_email");
			    	if (docs.getString("by").equals(email_login)){
			    		%>
			    		<div class="card card-pin">
				            <img class="card-img" src="<%=docs.getString("url")%>" alt="Card image">
				            <div class="overlay">
				                <div class="more">
				                    <small class="d-block"><a class="btn btn-sm" href="<%=docs.getString("url")%>"><i class="fa fa-external-link"></i> View Image</a></small>
				                </div>
				            </div>
				        </div>
				        <%
			    	}
			    }
			    %>
            </div>
        </div>
        
        <h2 class="font-weight-bold title"> My Posts </h2>
        <div class="row">
            <div class="card-columns">
                <jsp:useBean id="myPost" class="edu.stts.Post_Bean"></jsp:useBean>
                <%  
                List<QueryDocumentSnapshot> tPosts = myPost.getDocuments();
                for(QueryDocumentSnapshot docs:tPosts){
                    String email_login =(String) session.getAttribute("login_email");
                    if (docs.getString("by").equals(email_login)){
                        %>
                        <div class="card card-pin">
                            <img class="card-img" src="<%=docs.getString("url")%>" alt="Card image">
                            <div class="overlay">
                                <div class="more">
                                    <small class="d-block"><a class="btn btn-sm" href="<%=docs.getString("url")%>"><i class="fa fa-external-link"></i> View Image</a></small>
                                </div>
                            </div>
                        </div>
                        <%
                    }
                }
                %>
            </div>
        </div>
    </div>
        
   </main>

    
    <footer class="footer pt-5 pb-5 text-center">
    <div class="container">
            <!--
              All the links in the footer should remain intact.
              You may remove the links only if you donate:
              https://www.wowthemes.net/freebies-license/
            -->
          <p><span class="credits font-weight-bold">        
            <a target="_blank" class="text-dark" href="https://www.wowthemes.net/pintereso-free-html-bootstrap-template/"><u>Pintereso Bootstrap HTML Template</u> by WowThemes.net.</a>
          </span>
          </p>
        </div>
    </footer>    
</body>
    
</html>